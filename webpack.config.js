'use strict';

// Modules
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

let ENV = process.env.npm_lifecycle_event;
let isProd = ENV === 'build';
console.log("ENV: \x1b[46m%s\x1b[0m ", ENV);

module.exports = function makeWebpackConfig() {

  let config = {};
  config.entry = {
    app: './src/app/app.js'
  };

  config.output = {
    path: __dirname + '/dist',
    publicPath: isProd ? '/' : 'http://localhost:8080/',
    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js'
  };



  if (isProd) {
    config.devtool = 'source-map';
  }
  else {
    config.devtool = 'eval-source-map';
  }


  // Initialize module
  config.module = {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',          
          use: [
            { loader: 'css-loader' },
            { loader: 'sass-loader' },
            { loader: 'postcss-loader', options: { parser: 'postcss-scss'} }
          ],
        })
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        loader: 'file-loader'
      },
      {
        test: /\.html$/,
        loader: 'raw-loader'
      }]
  };


  config.plugins = [
    new webpack.LoaderOptionsPlugin({
      test: /\.scss$/i,
      options: {
        postcss: {
          plugins: [autoprefixer]
        }
      }
    })
  ];


  config.plugins.push(
    new HtmlWebpackPlugin({
      template: './src/public/index.html',
      inject: 'body'
    }),

    new ExtractTextPlugin({ filename: isProd ? 'css/[name]-[hash].css' :  'css/[name].bundle.css', /*disable: !isProd,*/ allChunks: true })
  )



  if (isProd) {
    config.plugins.push(
      new CleanWebpackPlugin(['dist'], {
        root: __dirname,
        verbose: true,
        dry: false
      }),

      new webpack.NoEmitOnErrorsPlugin(),

      //new webpack.optimize.UglifyJsPlugin(),

      new CopyWebpackPlugin([{
        from: __dirname + '/src/public'
      }])
    )
  }

  config.devServer = {
    contentBase: __dirname + "/src/public",
    stats: 'minimal'
  };

  return config;
}();
