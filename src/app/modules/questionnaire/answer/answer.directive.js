
import { answerTypes } from "../question.model";

export default class Answer {
    constructor() {
        this.template = require('./answer.directive.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = true;
        this.controller = DirectiveController;
        this.controllerAs = 'vm';
        this.bindToController = {
            answer: '=answer', //pass questions list with two-way-binding operator(equal).
        };
    }

    link(scope, attrs) {
        //
    }
}



class DirectiveController {
    constructor($rootScope, $scope) {
        this.$onInit = () => {
            this.answerType = this.answerTypes(this.answer);
            this.$scope = $scope;
        }
    }

    answerTypes (answer) {
        return {
            multiple: answer => answer.type === answerTypes.MULTIPLE && answer.choices.length > 2,
            double: answer => answer.type === answerTypes.MULTIPLE && answer.choices.length === 2,
            multiLine: answer => answer.type === answerTypes.TEXT && answer.multilineAllowed,
            singleLine: answer => answer.type === answerTypes.TEXT && !answer.multilineAllowed
        };
    };

    dispatchAnswer(selectedChoice){
        //Mark the Choice
        this.markChoice(selectedChoice);

        //Set the answer (Only for multiple and Double answer)
        this.setMultipleAnswer(selectedChoice);


        this.navigateQuestion();
    }

    markChoice(selectedChoice){
        if(this.answer.multipleChoiceAllowed){
            selectedChoice.selected = !selectedChoice.selected;
        }  else{
            this.answer.choices.forEach(choice => choice.selected = false);
            selectedChoice.selected = true;
        }
    }

    setMultipleAnswer(){ 
        this.answer.userAnswer = 
        this.answer.multipleChoiceAllowed ?
        this.answer.choices.map(choice => choice.selected) :
        this.answer.choices.find(choice => choice.selected);
    }

    handleTextQuestion(event){
        var keyCode = event.which || event.keyCode;
        if (keyCode === 13) {
            this.navigateQuestion();
        }
    }

    navigateQuestion(){
        this.$scope.$emit("question:anwer_selected", this.answer);
    }

}

DirectiveController.$inject = ['$rootScope', '$scope'];