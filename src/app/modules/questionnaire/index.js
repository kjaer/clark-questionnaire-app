'use strict';

import './questionnaire.scss';

import angular from 'angular';

import routing from './questionnaire.routes';
import QuestionNavigation from './navigation/navigation.directive';
import QuestionAnimation from './question.animation.directive';
import Question from './question/question.directive';
import Answer from './answer/answer.directive';
import QuestionnaireController from './questionnaire.controller';
import questionnaireService from '../../services/questionnaire.service';
import questionnaireFactory from '../../services/questionnaire.factory';

import Model from './question.model';

const uiRouter = require('@uirouter/angularjs').default

export default angular.module('app.questionnaire', [uiRouter, questionnaireService, questionnaireFactory])
  .config(routing)
  .controller('QuestionnaireController', QuestionnaireController)
  .directive('questionNavigation', () => new QuestionNavigation)
  .directive('questionAnimation', () => new QuestionAnimation)
  .directive('question', () => new Question)
  .directive('answer', () => new Answer)
  .name;