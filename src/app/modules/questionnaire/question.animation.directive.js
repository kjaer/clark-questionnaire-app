export default class QuestionAnimation {
    constructor() {
        this.restrict = 'A'; // A restriction is for using via attirbute
        this.scope = true;
    }

    link($scope, element, attrs, controller, transcludeFn) {

        //new AnimationLink($scope, element, attrs, controller, transcludeFn)

        $scope.$on("question:next_question", (event, data) => {
            element.removeClass("prev");
            element.addClass("next");            
        });

        $scope.$on("question:prev_question", (event, data) => {
            element.removeClass("next");
            element.addClass("prev");
        });
    }

}


class AnimationLink {
    constructor($scope, element, attrs, controller, transcludeFn) {
        this.$scope = $scope;
        this.element = element;

        $scope.$on("animation:next_question", (event, data) => {
            element.addClass("question-animating next");

            let nextQuestionElement = document.querySelectorAll(".next-question")[0];
            let eventHandler = animationEndEventListener.bind(this)

            nextQuestionElement.addEventListener("animationend", eventHandler, false)

            function animationEndEventListener(event){
                element.removeClass("question-animating next");
                this.fireNextQuestionEndEvent();
                nextQuestionElement.removeEventListener("animationend", eventHandler, false);
                this.$scope.$digest();
            }

        });


        $scope.$on("animation:prev_question", (event, data) => {

        });
    }

    fireNextQuestionEndEvent() {
        this.$scope.$emit("animation:next_question_end", {});
    }

    firePrevQuestionEndEvent() {
        this.$scope.$emit("animation:prev_question_end", {});
    }
}
