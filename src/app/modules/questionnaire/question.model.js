'use strict';

export class MultipleChoiceAnswerModel{
    constructor(rawQuestion){
        this.questionIdentifier = rawQuestion.identifier;
        this.type = rawQuestion.question_type;
        this.multipleChoiceAllowed = typeof rawQuestion.multiple === "string" ? JSON.parse(rawQuestion.multiple) : rawQuestion.multiple;
        this.choices = Array.from(rawQuestion.choices);
        this.userAnswer = null;
    }
}

export class TextAnswerModel{
    constructor(rawQuestion){
        this.questionIdentifier = rawQuestion.identifier;
        this.type = rawQuestion.question_type;        
        this.multilineAllowed = rawQuestion.multiline;
        this.userAnswer = null;        
    }
}

const questionTypes = [
    {type :"multiple-choice", answerModel: MultipleChoiceAnswerModel},
    {type :"text", answerModel: TextAnswerModel}
];

export default class QuestionModel {
    constructor(rawQuestion){
        this.type = rawQuestion.question_type;
        this.identifier = rawQuestion.identifier;
        this.questionSentence = rawQuestion.headline;
        this.description = rawQuestion.description;
        this.required = rawQuestion.required;
        this.jumps = Array.from(rawQuestion.jumps);
        this.answer = this.createAnswerModel(rawQuestion);
    }

    createAnswerModel(question){
        let Model = (questionTypes.find( questionType => questionType.type === question.question_type)).answerModel;
        return new Model(question);
    }
}

//Answer Type Enumaration
//In case of changing any property of enumaration, freeze the object.
export const answerTypes = Object.freeze({
    MULTIPLE :"multiple-choice",
    TEXT :"text"
});