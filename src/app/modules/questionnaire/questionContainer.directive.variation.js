 //.directive('questionContainer', QuestionContainer.questionContainerFactory)  
export default class QuestionContainer {
    constructor($timeout) {
        this.restrict = 'A'; // A restriction is for using via attirbute
        this.scope = true;
        this.$timeout = $timeout;
    }

    link(scope, element, attrs, controller, transcludeFn) {
        //This directive only handling key strokes.
        scope.$on("navigate:next_question", (event, data)=>{
            element.addClass("question-animating next");            
            
            let nextQuestionElement = document.querySelectorAll(".next-question");
            
            nextQuestionElement[0].addEventListener("animationend", (event)=>{
                scope.$emit("navigate:next_question_set")
            });
            
        })
    }

    static questionContainerFactory($timeout){
        QuestionContainer.instance = new QuestionContainer($timeout);
        return QuestionContainer.instance;
    }
}

QuestionContainer.questionContainerFactory.$inject= ["$timeout"];
