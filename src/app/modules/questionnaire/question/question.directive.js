

export default class Question {
    constructor() {
        this.template = require('./question.directive.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = true;
        this.controller = DirectiveController;
        this.controllerAs = 'vm';
        this.bindToController = {
            question: '=question'
        };
    }

    link(scope, attrs) {
        //
    }
}

class DirectiveController {
    constructor($rootScope, $scope) {
        this.$onInit = () => {
            this.$rootScope = $rootScope;
            this.$scope = $scope;

            this.hasDescription = !!this.question.description;
            
            //Tell navigation that question is required to answer or not;
            this.fireQuestionIsRequiredEvent();
            
            this.registerEvents();
        }
    }

    registerEvents() {
        //SUBSCRIBER
        this.$scope.$on("question:anwer_selected", (event, data) => {

            //Check question has any jump dependency
            if (this.question.jumps.length > 0) {
                this.fireJumpQuestionEvent(data.userAnswer);
            } else {
                this.fireNextQuestionEvent();
            }
        });
    }

    fireJumpQuestionEvent(userAnswer) {
        let jump = this.question.jumps.reduce((allJumpers, jumper) => {
            let isConditionFulfilled = jumper.conditions.find((condition) => {
              return condition.value === userAnswer.value;
          });
          
          return !!isConditionFulfilled ? jumper : allJumpers;
          
        } , {});

        //FIRE
        this.$scope.$emit("question:jump_to_question", { jumpQuestionId: jump.destination.id });
    }
    
    fireNextQuestionEvent() {
        //FIRE
        this.$scope.$emit("question:next_question", this.question);
    }

    fireQuestionIsRequiredEvent(){
        //FIRE
        let validation = this.question.required && !this.question.answer.userAnswer;
        this.$rootScope.$emit("question:is_required", validation);
    }

}

DirectiveController.$inject = ['$rootScope', '$scope'];