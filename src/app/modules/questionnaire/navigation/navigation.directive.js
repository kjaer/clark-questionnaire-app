

export default class QuestionNavigation {
    constructor() {
        this.template = require('./navigation.directive.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = true;
        this.controller = DirectiveController;
        this.controllerAs = 'vm';
        this.bindToController = {
            questions: '=questions',
            questionIndex: '=questionIndex'
        };
    }

    link(scope, attrs) {
        //
    }
}

class DirectiveController {
    constructor($rootScope, $scope) {
        this.$onInit = () => {
            this.$rootScope = $rootScope;
            this.$scope = $scope;

            this.totalQuestionCount = this.questions.length;
            this.nextButtonBlockOverride; //default value is undefined

            this.registerEvents();
        }
    }

    prevButtonBlock() {
        return this.questionIndex <= 1;
    }

    nextButtonBlock(override = undefined) {
        return override ?  override : this.questionIndex >= this.questions.length;
    }

    nextQuestion() {
        //FIRE
        this.$scope.$emit("question:next_question", {});
    }

    previousQuestion() {
        //FIRE
        this.$scope.$emit("question:prev_question", {});
    }

    registerEvents() {
        this.$rootScope.$on("question:is_required", (event, isRequired) => {
            this.nextButtonBlockOverride = isRequired;
        })
    }

}

DirectiveController.$inject = ['$rootScope', '$scope'];