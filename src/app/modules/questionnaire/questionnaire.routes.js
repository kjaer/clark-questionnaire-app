'use strict';

routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
    .state('questionnaire', {
      url: '/questionnaire',
      template: require('./questionnaire.html'),
      controller: 'QuestionnaireController',
      controllerAs: 'questionnaire',
      resolve: {
        questionnaireData: ['questionnaireFactory', (questionnaireFactory) => {
          return questionnaireFactory.retreiveQuestions()
        }]
      }
    })
    .state('complete', {
      url: '/questionnaire-complete',
      template: require('./questionnaire_finish.html')
    });
}