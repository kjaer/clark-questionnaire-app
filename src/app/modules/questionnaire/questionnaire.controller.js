'use strict';
import QuestionModel from "./question.model";

export default class QuestionnaireController {
  constructor($rootScope, $scope, $timeout, $state, questionnaireData, sessionStorageManager) {
    this.$onInit = () => {
      this.$scope = $scope;
      this.$state = $state;
      this.$timeout = $timeout;

      this.questionnaire = questionnaireData;

      this.questions = this.questionnaire.questions.map(question => new QuestionModel(question)); //holds the modelled questions
      this.questionList = [this.questions[0]]; //holds the questions to show only user.
      this.questionIndex = 1;

      this.registerEvents();
      //sessionStorageManager.put({activeQuestionnaire:this.questionnaire.name,answeredQuestions:[], leftQuestionId:""});
    }
  }

  registerEvents() {

    this.$scope.$on("question:next_question", (event, data) => {
      //this block need to know user reach last of the question and don't send it forward.

      let nextQuestionIndex = this.questions.indexOf(this.questionList[0]) + 1;

      if (nextQuestionIndex >= this.questions.length) {
        this.$state.go('complete');
        return;
      }

      this.$timeout(() => {
        this.questionList.push(this.questions[nextQuestionIndex]);
        this.questionList.shift();
        this.questionIndex = nextQuestionIndex + 1;
      }, 200);

    });


    this.$scope.$on("question:prev_question", (event, data) => {
      //this block need to know user reach first of the question and don't send it backward

      let prevQuestionIndex = this.questions.indexOf(this.questionList[0]) - 1;

      if (prevQuestionIndex < 0) {
        return;
      }

      this.$timeout(() => {
        this.questionList.pop();
        this.questionList.push(this.questions[prevQuestionIndex]);
        this.questionIndex = prevQuestionIndex + 1;
      }, 200);

    });


    this.$scope.$on("question:jump_to_question", (event, data) => {
      let targetQuestionIndex;
      let targetQuestion = this.questions.find( (question, index) => {
        targetQuestionIndex = index;
        return question.identifier === data.jumpQuestionId;
      });

      this.$timeout(() => {
        this.questionList.pop();
        this.questionList.push(targetQuestion);
        this.questionIndex = targetQuestionIndex + 1;
      }, 200);
        
    });
  }

}

QuestionnaireController.$inject = ['$rootScope', '$scope', '$timeout', '$state', 'questionnaireData', '$sessionStorage'];