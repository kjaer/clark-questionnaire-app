'use strict';

routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      template: require('./home.html'),
      controller: 'HomeController',
      controllerAs: 'home',
      resolve: {
        questionnaireData: ['questionnaireFactory', (questionnaireFactory) => {
          return questionnaireFactory.retreiveQuestions()
        }]
      }
    });
}