'use strict';

import './home.scss';

import angular from 'angular';

import routing from './home.routes';
import HomeController from './home.controller';
import questionnaireService from '../../services/questionnaire.service';
import questionnaireFactory from '../../services/questionnaire.factory';

const uiRouter = require('@uirouter/angularjs').default

export default angular.module('app.home', [uiRouter, questionnaireService, questionnaireFactory])
  .config(routing)
  .controller('HomeController', HomeController)
  .name;