'use strict';

import angular from 'angular';

let _questionnaire = undefined;

class QuestionnaireFactoryService {
  constructor($q, questionnaire) {
    this.q = $q;
    this.questionnaireSvc = questionnaire;
  }


  retreiveQuestions(){
    if(!!_questionnaire) {
      let deferred = this.q.defer();
      deferred.resolve(_questionnaire);
      return deferred.promise;

    }else{
        
      return this.questionnaireSvc.getQuestions().then((response) => {
        _questionnaire = response.data.questionnaire;
        return _questionnaire;
      })
    }
  }
  static questionnaireFactory($q, questionnaire){
    return new QuestionnaireFactoryService($q, questionnaire);
  }
}

QuestionnaireFactoryService.questionnaireFactory.$inject = ['$q', 'questionnaire'];



export default angular.module('factory.questionnaire', [])
  .factory('questionnaireFactory', QuestionnaireFactoryService.questionnaireFactory)
  .name;