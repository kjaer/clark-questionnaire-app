'use strict';

import angular from 'angular';

let _questionnaire = undefined;

class Questionnaire {
  constructor($http) {
    this.$http = $http;
  }

  getQuestions() {
    //return this.$http.get('http://www.mocky.io/v2/5a34e85f2f0000e103e251a0'); //multiple-conditions
    return this.$http.get('http://www.mocky.io/v2/5a3633212f0000b402127a65'); //has some required questions
    //return this.$http.get('http://www.mocky.io/v2/5a2ecdb82e0000a42e278fa6');
  }
  
}

Questionnaire.$inject = ['$http'];

export default angular.module('services.questionnaire', [])
  .service('questionnaire', Questionnaire)
  .name;