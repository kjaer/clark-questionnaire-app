import "./app.scss";

import angular from 'angular';
import nganimate from 'angular-animate';
import ngsessionstorage from 'angular-sessionstorage';
import home from './modules/home/index';
import questionnaire from './modules/questionnaire/index';

import routing from './app.config';

const uiRouter = require('@uirouter/angularjs').default

angular.module('app', [uiRouter, nganimate, ngsessionstorage, home, questionnaire])
  .config(routing);