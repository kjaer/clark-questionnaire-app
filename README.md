# Clark Questionnaire Web App


This challenge based on this [angular.js-webpack](https://github.com/preboot/angularjs-webpack) repository and uses some of the this [Milligram CSS framework](https://github.com/milligram/milligram) parts.

### Quick start

```bash
# install the dependencies with npm
$ npm install

# start the server
$ npm start
```
It will start a local server using `webpack-dev-server` which will watch, build (in-memory), and reload for you. The port will be displayed to you as `http://localhost:8080`.

Go to [http://localhost:8080](http://localhost:8080) in your browser.

